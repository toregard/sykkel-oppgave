
## Innledning
https://spring.io/guides/gs/spring-boot-docker/


Dette er en spring boot applikasjon, rest tjeneste

**_http://localhost:8080/swagger-ui.html_**
e09d662fb490f1ec999486817153ee27
http://localhost:8080/v2/api-docs


## Min tenkte arkitektur for hele løsningen
* Data hentes / sendes fra stasjonene. Hendelseorientert, reactive streaming, erstatte RestTemplate class med Spring WebFlux og WebClient class. 
* Reactive WebSocket Clients, dette er hva jeg hadde prøvd ut hvis jeg skulle gjøre oppgaven en gang til, se https://stackify.com/reactive-spring-5/
* Dataene samles et sentralt sted
* Dataene gjøres tilgjengelig i en nosql "ustrukturert" database (mongodb, elastisksearch ) 
* https://developer.oslobysykkel.no/api løsningen eksponerer et HAL formatert grensesnitt mot kunde (meg)
 
## Kommentarer om API'et

### HAL basert grensesnitt
Som kunde ønsker jeg å traverse gjennom ressursene, da jeg jakter på spesifikke stasjoner

Jeg ønsker å få presentert alle ressurser også på dette (stations, Status, Availability)
Eks. :
* ../stations/157    --> en station ressurs
* ../stations/157/bounds  --> alle i station
* ../stations/status --> alle i station
..

### stations rest API
Tilby spørring. Jeg er bare interessert i navnene på stasjonene.

### Savner en løsning å abonnere på endringer
Meningsløst å tilby et stations API som lister alt mellom himmel og jord.

Dette kan vi eventuelt diskutere videre. Studier av bruker mønster

## Løsningen kan startes ved å kalle maven 

* mvn spring-boot:run

## Løsningen kan testet ved å kalle http get
* http://localhost:8080/stations?identifier=LEGGINNKODEHER

## Valg
Spiller rollen  backend utvikler som skal laget et layer over sykkel løsningen.

Det er dynamisk kall til availability rest tjenesten

https://oslobysykkel.no/api/v1/stations/availability

## Begrensning/videre arbeide 

### https://oslobysykkel.no/api/v1/stations problemer
Greide ikke å handtere helt unmarshalling mot "https://oslobysykkel.no/api/v1/stations" med den tekniske løsningen jeg valgte.
Jeg fikk bar id. Men tekst kom ikke med. 

Jeg valgte derfor å lese fra fra fil. Det gikk fint.

Disse dataene kunne vært hendelsesorientert fordi de endres skjeldent.

Så kunne f.eks kunde selv ta en avgjørelse på handteringene av dette.

Sjekker ikke mot stasjoner mht stengte stasjoner. Men jeg har implementert endpointet.

## Forbedringer
https://spring.io/guides/gs/async-method/ ville eventuelt vært noe å se på.

Man kunne også lagt opp et løp der man kjører flere tråder og asynkront.

Java har sine begrensninger. 

 
 Tore Gard Andersen

