package pakken;

import java.io.Serializable;

/**
 * Abstract super-class for base data-types.
 *
 * @author Steinar Haugen
 * @since COS 9.0

 */
public abstract class AbstractType implements Serializable, Comparable {

    private static final long serialVersionUID = 1L;

    /**
     * Compares this object to the specified object. The result is
     * true if and only if the argument is not null and is of the same
     * run-time type as this object, and contains the same integer
     * value as this object.
     *
     * @param obj  the object to compare with.
     * @return     <code>true</code> if the objects are the same;
     *             <code>false</code> otherwise.
     */
    public boolean equals(Object obj){
        return this == obj || (obj != null && obj.getClass() == getClass() && compareTo(obj) == 0);
    }

    /**
     * Returns whether this object is less than another.
     *
     * @param other the object to compare with
     * @return <code>true</code> if and only if <code>compareTo(other) &lt; 0</code>
     */
    public boolean isLessThan(AbstractType other)
    {
        return compareTo(other) < 0;
    }

    /**
     * Returns whether this object is greater than another.
     *
     * @param other the object to compare with
     * @return <code>true</code> if and only if <code>compareTo(other) &gt; 0</code>
     */
    public boolean isGreaterThan(AbstractType other)
    {
        return compareTo(other) > 0;
    }

    /**
     * Compares this object with the specified objects. The result is true if
     * and only if it is inclusively between the given lower and upper bounds; i.e. <br>
     * <code>lower &lt;= this &lt;= upper</code>. <p>
     *
     * @param lower  lower bounding object
     * @param upper  upper bounding object
     * @return       whether this object is inclusively between given lower and upper bounds
     *
     * @see #isBetween(AbstractType, boolean, AbstractType, boolean)
     * @exception  ClassCastException  if either lower or upper is of a type that is
     *                                 not comparable with this class.
     */
    public boolean isBetween(AbstractType lower, AbstractType upper)
    {
        return (compareTo(lower)>=0 && compareTo(upper)<=0);
    }

    /**
     * Compares this object with the specified objects. The result is true if
     * and only if it is between the given lower and upper bounds and inclusive flags; i.e. <br>
     * with both flags set to <code>true: lower &lt;= this &lt;= upper</code>. <br>
     * with both flags set to <code>false: lower &lt; this &lt; upper</code>. <br>
     *
     * @param lower           lower bounding object
     * @param lowerInclusive  whether lower bound is inclusive or exclusive
     * @param upper           upper bounding object
     * @param upperInclusive  whether upper bound is inclusive or exclusive
     * @return                whether this object is between given upper and lower bounds
     *
     * @see #isBetween(AbstractType, AbstractType)
     * @exception  ClassCastException  if either lower or upper is of a type that is
     *                                 not comparable with this class.
     */
    public boolean isBetween(AbstractType lower, boolean lowerInclusive, AbstractType upper, boolean upperInclusive)
    {
        if (lowerInclusive && upperInclusive)
            return compareTo(lower)>=0 && compareTo(upper)<=0;

        if (lowerInclusive && !upperInclusive)
            return compareTo(lower)>=0 && compareTo(upper)<0;

        if (!lowerInclusive && upperInclusive)
            return compareTo(lower)>0 && compareTo(upper)<=0;

        return compareTo(lower)>0 && compareTo(upper)<0;
    }

}

