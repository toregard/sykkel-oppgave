package pakken;

import com.google.common.collect.Sets;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class Streaming {
    Collection<ProductOfferId> result = Sets.newHashSet();
    List<Long> result2 = new ArrayList<>();


    Employee[] createEmployees() {
        List<Employee> liste = new ArrayList<>();
        long akk = 0;
        for (int i = 0; i < 20000; i++) {
            liste.add(new Employee(String.format("data%d", i), 20000 + 1000 * i));
            akk = 20000 + 1000 * i;
        }
        Employee[] employees = new Employee[liste.size()];
        return liste.toArray(employees);
    }

    @Test
    public void ny1() {
        Collection<Long> result = Sets.newHashSet();
        Employee[] data = createEmployees();
        Set<Long> empNames = Arrays
                .stream(data)
                .map(Employee::getSalary)
                .peek(e -> System.out.println(e))
                .collect(Collectors.toSet());


    }

    @Test
    public void ny() {
        Employee[] employees = createEmployees();
        final long akkresult = 20000 + 1000 * 20000;
        long t1, t2;
        t1 = System.currentTimeMillis();
        Employee[] arrayListe =
                Arrays
                        .stream(employees)
                        .parallel()
                        .filter(v -> v.getSalary() >= 40000 && v.getSalary() < akkresult)
                        .peek(v -> aaaa(v.getSalary()))
                        .toArray(Employee[]::new);
        t2 = System.currentTimeMillis();
        System.out.println("Sequential Stream Time Taken?= " + (t2 - t1) + "\n");
    }

    void aaaa(long id) {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result2.add(id);
    }

    // @Test
    public void a() {
        long t1, t2;
        List<Employee> eList = new ArrayList<Employee>();
        for (int i = 0; i < 100; i++) {
            eList.add(new Employee("A", 20000));
            eList.add(new Employee("B", 3000));
            eList.add(new Employee("C", 15002));
            eList.add(new Employee("D", 7856));
            eList.add(new Employee("E", 200));
            eList.add(new Employee("F", 50000));
        }

        /***** Here We Are Creating A 'Sequential Stream' & Displaying The Result *****/
        t1 = System.currentTimeMillis();
        System.out.println("Sequential Stream Count?= " +
                eList
                        .stream()
                        .filter(e -> e.getSalary() > 15000).count());

        t2 = System.currentTimeMillis();
        System.out.println("Sequential Stream Time Taken?= " + (t2 - t1) + "\n");

        /***** Here We Are Creating A 'Parallel Stream' & Displaying The Result *****/
        t1 = System.currentTimeMillis();
        System.out.println("Parallel Stream Count?= " + eList.parallelStream().filter(e -> e.getSalary() > 15000).count());

        t2 = System.currentTimeMillis();
        System.out.println("Parallel Stream Time Taken?= " + (t2 - t1));
        assertThat(true).isEqualTo(true);

        /*

        Stream<ProductOffer> stream = Arrays.stream(mandatoryActivationProductOffers);
//        stream.parallel().filter()
         */
    }
}
