package pakken;

import java.util.Collection;
import java.util.Iterator;

/**
 * Represents the ID of a productoffer.
 *
 * @author Carl Kristian Eriksen
 * @author Ole-Martin M�rk
 * @since COS 19.0
 */
public class ProductOfferId extends IntegerType {

    public static final ProductOfferId VIDEREKOBLING_TIL_MOBLISVAR = fromInteger(663);
    public static final ProductOfferId MOBILSVAR_RING_TILBAKE = fromInteger(655);
    public static final ProductOfferId MOBILSVAR = fromInteger(692);
    public static final ProductOfferId MOBILSVAR_SP = fromInteger(1214);
    public static final ProductOfferId TRAADLOS_SONE = fromInteger(693);
    public static final ProductOfferId TELENOR_WAP = fromInteger(659);
    public static final ProductOfferId ABONNENTSTYRT_SPERRING = fromInteger(1099);
    public static final ProductOfferId DATA_TWIN = fromInteger(679);
    public static final ProductOfferId BEDRIFT_LARGE = fromInteger(356);
    public static final ProductOfferId PRIVAT = fromInteger(1);
    public static final ProductOfferId TALKMORE_SP_PRIORITET = fromInteger(390);
    public static final ProductOfferId HELLO_NORWAY_SP_PRIORITET = fromInteger(391);
    public static final ProductOfferId PHONERO_SP_PRIORITET = fromInteger(392);
    public static final ProductOfferId MOBILT_BREDBAAND_1GB = fromInteger(1034);

    /** Tvilling sim cards */
    public static final ProductOfferId TELENOR_TVILLING_BEDRIFT = fromInteger(698);
    public static final ProductOfferId TALKMORE_TVILLING = fromInteger(807);
    public static final ProductOfferId HELLO_TVILLING = fromInteger(820);
    public static final ProductOfferId TELENOR_TVILLING_PRIVAT = fromInteger(821);
    public static final ProductOfferId DJUICE_TVILLING_PRIVAT = fromInteger(822);
    public static final ProductOfferId DIPPER_TVILLING = fromInteger(854);
    public static final ProductOfferId PHONERO_TVILLING = fromInteger(855);
    public static final ProductOfferId CHILI_TVILLING = fromInteger(1210);

    /** Datakort sim cards */
    public static final ProductOfferId DATAKORT = fromInteger(678);
    public static final ProductOfferId DIPPER_DATAKORT = fromInteger(815);
    public static final ProductOfferId HELLO_DATAKORT = fromInteger(819);
    public static final ProductOfferId PHONERO_DATAKORT = fromInteger(856);
    public static final ProductOfferId TALKMORE_DATAKORT = fromInteger(857);
    public static final ProductOfferId CHILI_DATAKORT = fromInteger(1207);

    /** Datakort2 sim cards */
    public static final ProductOfferId DATAKORT2 = fromInteger(688);
    public static final ProductOfferId DIPPER_DATAKORT2 = fromInteger(1203);
    public static final ProductOfferId HELLO_DATAKORT2 = fromInteger(1204);
    public static final ProductOfferId PHONERO_DATAKORT2 = fromInteger(1206);
    public static final ProductOfferId TALKMORE_DATAKORT2 = fromInteger(1205);
    public static final ProductOfferId CHILI_DATAKORT2 = fromInteger(1208);

    /** MobilFax product offer ids */
    public static final ProductOfferId MOBILFAX = fromInteger(694);
    public static final ProductOfferId MOBILFAX_SP = fromInteger(1215);

    private static final long serialVersionUID = 1L;

    /**
     * The product offer id for Vennenett.
     * @deprecated Will be deleted in COS49
     */
    public static final ProductOfferId VENNENETT = ProductOfferId.fromString("4001");

    /**
     * The product offer id for Move standard agreement offer
     */
    public static final ProductOfferId MV_MOBILT_BEDRIFTSNETT = ProductOfferId.fromString("401");

    /**
     * The product offer id for Top100K
     */
    public static final ProductOfferId TOPP_100K_ROTPRODUKT = ProductOfferId.fromString("706");

    /**
     * The product offer id for Cloud Super Market
     */
    public static final ProductOfferId MICROSOFT_SKYTJENESTER = ProductOfferId.fromString("1201");

    /**
     * The product offer id for Move member offer.
     */
    public static final ProductOfferId MV_MOBILT_BEDRIFTSNETT_MEMBER = ProductOfferId.fromString("421");

    /**
     * The product offer id for FriFamilie standard agreement offer.
     */
    public static final ProductOfferId FRI_FAMILIE_OFFER = ProductOfferId.fromString("600");

    /**
     * The product offer id for FriFamilie postpaid member.
     */
    public static final ProductOfferId FRI_FAMILIE_POSTPAID_FULLVERDIG = ProductOfferId.fromString("602");

    /**
     * The product offer id for FriFamilie corporate member.
     */
    public static final ProductOfferId FRI_FAMILIE_BEDRIFT_FULLVERDIG = ProductOfferId.fromString("603");

    /**
     * The product offer id for FriFamilie prepaid member.
     */
    public static final ProductOfferId FRI_FAMILIE_PREPAID_FULLVERDIG = ProductOfferId.fromString("604");

    /**
     * The product offer id for FriFamilie fixed member.
     */
    public static final ProductOfferId FRI_FAMILIE_FASTNETT_FULLVERDIG = ProductOfferId.fromString("605");

    /**
     * A product offer representing a Mobilt Bedriftsnett queue
     */
    public static final ProductOfferId MB_QUEUE = ProductOfferId.fromString("404");

    /**
     * A product offer representing a Mobilt Bedriftsnett campaign on queue
     */
    public static final ProductOfferId MB_CAMPAIGN_QUEUE = ProductOfferId.fromString("70240");

    /**
     * A product offer representing a Mobilt Bedriftsnett PBX numberseries
     */
    public static final ProductOfferId MB_PBX_NUMBERSERIES = ProductOfferId.fromString("413");

    /**
     * A product offer representing screening mode.
     */
    public static final ProductOfferId MB_SCREENING_MODE = ProductOfferId.fromString("462");

    /**
     * A product offer representing a Mobilt Bedriftsnett PBX
     */
    public static final ProductOfferId MB_PBX = ProductOfferId.fromString("443");

    /**
     * A product offer representing a Mobilhandel
     */
    public static final ProductOfferId MOBILHANDEL = ProductOfferId.fromString("5178");
    /**
     * A product offer representing a PBX CAC.
     */
    public static final ProductOfferId MB_PBX_CAC = ProductOfferId.fromString("461");
    /**
     * A product offer representing a IPT Internet Access.
     */
    public static final ProductOfferId MB_PBX_IPTIA = ProductOfferId.fromString("476");
    /**
     * A product offer representing a MBN fixed main number
     */
    public static final ProductOfferId MBN_FIXED_MAIN_NUMBER_ADVANCED = ProductOfferId.fromString("487");

    /**
     * A product offer representing main sim card for consumer subscriptions
     */
    public static final ProductOfferId MAIN_SIM_CARD_ADDON_PRODUCT_PREPAID = ProductOfferId.fromString("801");

    /**
     * A product offer representing main sim card for business subscriptions
     */
    public static final ProductOfferId MAIN_SIM_CARD_ADDON_PRODUCT_BUSINESS = ProductOfferId.fromString("802");

    /**
     * A product offer representing main sim card for consumer subscriptions
     */
    public static final ProductOfferId MAIN_SIM_CARD_ADDON_PRODUCT_CONSUMER = ProductOfferId.fromString("803");

    /**
     * A product offer representing main sim card for priority subscriptions
     */
    public static final ProductOfferId MAIN_SIM_CARD_ADDON_PRODUCT_PRIORITY = ProductOfferId.fromString("804");

    /**
     * A product offer representing main sim card for prepaid djuice subscriptions
     */
    public static final ProductOfferId MAIN_SIM_CARD_ADDON_PRODUCT_DJUICE_PREPAID = ProductOfferId.fromString("809");

    /**
     * A product offer representing main sim card for postpaid djuice subscriptions
     */
    public static final ProductOfferId MAIN_SIM_CARD_ADDON_PRODUCT_DJUICE_POSTPAID = ProductOfferId.fromString("810");

    /**
     * A product offer representing main sim card for M2M subscriptions
     */
    public static final ProductOfferId MAIN_SIM_CARD_ADDON_PRODUCT_M2M = ProductOfferId.fromString("811");

    /**
     * A product offer representing tvilling - Telenor Tviling Bedrift
     */

    public static final ProductOfferId SHARED_BUCKET_BUSINESS_ROOTOFFER = ProductOfferId.fromString("990");

    public static final ProductOfferId SHARED_BUCKET_M2M_ROOTOFFER = ProductOfferId.fromString("999");

    public static final ProductOfferId SHARED_DATA_ROOTOFFER = ProductOfferId.fromString("950");

    /**
     * Device/swap/insurance related product offer ids
     */
    public static final ProductOfferId DEVICE_ROOT_OFFER = ProductOfferId.fromInteger(770);

    public static final ProductOfferId SWAP_PLUS_INSURANCE = ProductOfferId.fromString("76345");

    public static final ProductOfferId SWAP_PLUS_INSURANCE_INCLUDED = ProductOfferId.fromString("76526");

    public static final ProductOfferId SWAP_SCREEN_INSURANCE = ProductOfferId.fromString("76334");

    public static final ProductOfferId SWAP_3_MONTH_CAMPAIGN = ProductOfferId.fromString("75729");

    /**
     * Family bonus related product offer ids
     */
    public static final ProductOfferId FAMILY_SAHRED_DATA_SUBSCRIPTION_OFFER = ProductOfferId.fromString("951");

    public static final ProductOfferId FAMILY_SAHRED_DATA_AGREEMENT_OFFER = ProductOfferId.fromString("950");


    /**
     * Trygg related product offer ids
     */
    public static final ProductOfferId TRYGG_SUBSCRIPTION_ID = ProductOfferId.fromString("76835");

    public static final ProductOfferId TRYGG_ID_CAT_8= ProductOfferId.fromString("76867");

    public static final ProductOfferId TRYGG_ID_CAMPAIGN = ProductOfferId.fromString("76894");

    public static final ProductOfferId TRYGG_ID_CAT_15 = ProductOfferId.fromString("76879");

    /**
     * MOVE logical/virtual number
     */
    public static final ProductOfferId MB_LOGISK_NUMMER = ProductOfferId.fromString("410");


    /**
     * Creates a new <code>ProductOfferId</code> object that represents the <code>Integer</code> value.
     *
     * @param value the id represented as an Integer.
     * @throws NullPointerException if <code>value == null</code>.
     */
    private ProductOfferId(Integer value) {
        super(value);
    }

    /**
     * Convenience factory method that creates a new ProductOfferId object, taking <code>null</code> values into account.
     *
     * @param value the underlying <code>Integer</code> value
     * @return the new ProductOfferId object, or <code>null</code> if the underlying Integer value is <code>null</code>.
     */
    public static ProductOfferId fromInteger(Integer value) {
        return value == null ? null : new ProductOfferId(value);
    }

    /**
     * Convenience factory method that creates a new ProductOfferId object, taking <code>null</code> values into account.
     *
     * @param value the underlying <code>String</code> value
     * @return the new ProductOfferId object, or <code>null</code> if the underlying String value is <code>null</code>.
     * @throws NumberFormatException if the <code>String</code> does not contain a parsable long.
     */
    public static ProductOfferId fromString(String value) {
        return value == null ? null : new ProductOfferId(Integer.valueOf(value));
    }

    /**
     * Convenience method for converting an array of Integer values to an array of ProductOfferId's.
     *
     * @param values the array of Integers to convert.
     * @return an array of ProductOfferId's.
     * @methodAuthor Ole J�rgen Aurebekk, Steinar Haugen
     * @since COS27
     */
    public static ProductOfferId[] fromIntegers(Integer... values) {
        if (values == null || values.length == 0) {
            return new ProductOfferId[0];
        }
        ProductOfferId[] ids = new ProductOfferId[values.length];
        for (int i = 0; i < values.length; i++) {
            ids[i] = ProductOfferId.fromInteger(values[i]);
        }
        return ids;
    }

    /**
     * Convenience method for converting an array of String values to an array of ProductOfferId's.
     *
     * @param values the array of Strings to convert.
     * @return an array of ProductOfferId's.
     * @methodAuthor Ole J�rgen Aurebekk, Steinar Haugen
     * @since COS27
     */
    public static ProductOfferId[] fromStrings(String[] values) {
        if (values == null || values.length == 0) {
            return new ProductOfferId[0];
        }
        ProductOfferId[] ids = new ProductOfferId[values.length];
        for (int i = 0; i < values.length; i++) {
            ids[i] = ProductOfferId.fromString(values[i]);
        }
        return ids;
    }

    /**
     * Converts a collection of productOfferIds to Integer[]
     *
     * @param productOfferIds the ids to convert
     * @return Integer[]. Empty array if input = null
     */
    public static Integer[] toIntegers(Collection<ProductOfferId> productOfferIds) {
        if (productOfferIds == null) {
            return new Integer[0];
        }

        Integer[] integers = new Integer[productOfferIds.size()];
        int index = 0;

        Iterator<ProductOfferId> iterator = productOfferIds.iterator();
        while (iterator.hasNext()) {
            integers[index++] = iterator.next().getValue();
        }

        return integers;
    }

    /**
     * Convenience method for converting array of productofferids to Integer[]
     *
     * @param productOffers productOffers to convert
     * @return Integer[]. Empty array if input = null
     * @methodAuthor Bj�rn Haakenstad
     * @since COS 32.0
     */
    public static Integer[] toInteger(ProductOfferId[] productOffers) {
        if (productOffers == null) {
            return new Integer[0];
        }
        Integer[] integers = new Integer[productOffers.length];
        for (int i = 0; i < productOffers.length; i++) {
            ProductOfferId productId = productOffers[i];
            integers[i] = toInteger(productId);
        }
        return integers;
    }

    /**
     * Convenience method for converting productOfferId into Integer
     *
     * @param productOfferId the productOfferId to convert
     * @return the converted Integer
     * @methodAuthor Bj�rn Haakenstad
     * @since COS 32.0
     */
    public static Integer toInteger(ProductOfferId productOfferId) {
        if (productOfferId == null) {
            return null;
        } else {
            return productOfferId.getValue();
        }
    }


}