package pakken;

import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class StreamHandleExceptionExample {
    private static final String ERROR_EXCEPTION = "Feiler for tallet 6";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     *  Filter integer 5 happy
     */
    @Test
    public void testStreamingHandleHappy() throws IOException {
        theOperation(Arrays.asList(1, 2, 3, 4, 5, 7));
        assertThat(true).isEqualTo(true);
    }

    /**
     * Exception on integer 6
     */
    @Test
    public void testStreamingHandleException() throws IOException {
        thrown.expect(IOException.class);
        thrown.expectMessage(ERROR_EXCEPTION);
        theOperation(Arrays.asList(1, 2, 3, 4, 5, 6, 7,8));
    }

    static boolean validatation(Integer integer) throws IOException {
        long threadId =Thread.currentThread().getId();
        System.out.println(String.format("validation of Thread: %d data: %d ",threadId,integer));
        //Exception 6
        if (integer.intValue() == 6) {
            throw new IOException(String.format("Feiler for tallet %d", integer.intValue()));
        }
        //Filter out 5
        if (integer.intValue() == 5) {
            return false;
        }

        return true;
    }

    /**
     * https://stackoverflow.com/questions/30164352/how-throw-the-custom-checked-exception-with-java8-lambda-expression
     * List<MyException> caughtExceptions = new ArrayList<>();
     *
     * @throws IOException
     */
    private void theOperation(List<Integer> integers) throws IOException {
        List<IOException> caughtExceptions = new ArrayList<>();
        Collection<Integer> result = Sets.newHashSet();

        try {
            result = integers
                    .parallelStream()
                    .filter(a ->
                            {
                                try {
                                    return validatation(a);
                                } catch (IOException e) {
                                    caughtExceptions.add(e);
                                    throw new RuntimeException(e);
                                }
                            }
                    )
                    .collect(Collectors.toSet());
        } catch (RuntimeException e) {
            if (caughtExceptions.size() > 0) {
                System.out.println("Exception on 6:*********");
                System.out.println("result = "+result);
                System.out.println(caughtExceptions.get(0));
                throw caughtExceptions.get(0);
            }
        }

            System.out.println("OK");
            System.out.println(String.format("Result %d", result.size()));
            result.stream().forEach(v -> System.out.println(v));


    }



//    @Test
//    public void exception2() {
//        List<Integer> integers = Arrays.asList(3, 9, 7, 0, 10, 20);
//        integers
//                .stream()
//                .peek(e -> System.out.println(e))
//                .forEach(handlingConsumerWrapper(i -> writeToFile(i), IOException.class));
//
//    }
//
//
//    static <T, E extends Exception> Consumer<T> handlingConsumerWrapper(
//            ThrowingConsumer<T, E> throwingConsumer, Class<E> exceptionClass) {
//
//        return i -> {
//            try {
//                throwingConsumer.accept(i);
//            } catch (Exception ex) {
//                try {
//                    E exCast = exceptionClass.cast(ex);
//                    System.err.println(
//                            "Exception occured : " + exCast.getMessage());
//                } catch (ClassCastException ccEx) {
//                    throw new RuntimeException(ex);
//                }
//            }
//        };
//    }
}
