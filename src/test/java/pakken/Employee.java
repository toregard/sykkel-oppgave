package pakken;

public class Employee {
    private String arg;
    private long salary;

    public Employee(String arg, long salary) {
        this.arg = arg;
        this.salary = salary;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }
}
