package no.oslokommune.toregard.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Rad {
private String navnPaaStativ;
private int antallTilgjengeligeLaaser;
private int antallLedigeSykler;
}
