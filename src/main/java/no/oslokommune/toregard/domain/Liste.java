package no.oslokommune.toregard.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Builder
@Getter
@Setter
public class Liste {
    Map<Integer,Rad> rader;
}
