package no.oslokommune.toregard.restcontroller;

import no.oslokommune.toregard.domain.Liste;
import no.oslokommune.toregard.endpoints.SykkelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/stations")
public class SyklerResource {
    private final SykkelService sykkelService;

    @Inject
    public SyklerResource(SykkelService sykkelService) {
        this.sykkelService=sykkelService;
    }

    @RequestMapping(
            produces = "application/json",
            method = RequestMethod.GET
    )
    public ResponseEntity<?> getSykkelStativer(@RequestParam(required = false) String identifier) {
        Liste liste = null;
        try {
            liste = sykkelService.list(identifier);
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(liste);
    }





}
