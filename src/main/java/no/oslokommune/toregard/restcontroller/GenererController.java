package no.oslokommune.toregard.restcontroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GenererController {
    @RequestMapping("/a")
    public void handleRequest() {
        throw new RuntimeException("test exception");
    }
}
