package no.oslokommune.toregard.restcontroller;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;

import java.io.File;

public class S3TransferManager {

    public S3TransferManager() {
    }

    public void transferfromCatalogStructurOnUnix(String baseCatalalog, String newBucketName) {
        AWSCredentials credentials = new BasicAWSCredentials("yabankyabank", "yabankyabank");
        AmazonS3 s3Client = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:9000", Regions.US_EAST_1.name()))
                .withPathStyleAccessEnabled(true)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();


        TransferManager xfer_mgr = TransferManagerBuilder.standard().withS3Client(s3Client).build();
        AmazonS3 s3Client2 =xfer_mgr.getAmazonS3Client();


        try {
            boolean recursive = true;

            MultipleFileUpload xfer = xfer_mgr.uploadDirectory(newBucketName, "hello", new File(baseCatalalog), recursive);

        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
    }
}
