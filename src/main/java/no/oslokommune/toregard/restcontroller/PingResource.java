package no.oslokommune.toregard.restcontroller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hallo")
public class PingResource {



    @GetMapping(value = "/ping")
    public String ping(){
        return this.getClass().getCanonicalName();
    }
}
