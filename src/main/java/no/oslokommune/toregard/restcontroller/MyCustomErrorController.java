package no.oslokommune.toregard.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
public class MyCustomErrorController implements ErrorController {
    private String errorPath="/error";

//    @RequestMapping(value="/error")
//    @ResponseBody
    public String handleError(HttpServletRequest request) {

       // if(request.getServletPath().endsWith("a")
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        return String.format("<html><body><h2>Error Page</h2><div>Status code: <b>%s</b></div>"
                        + "<div>Exception Message: <b>%s</b></div><body></html>",
                statusCode, exception==null? "N/A": exception.getMessage());
    }

    @RequestMapping(value="/error")
    ResponseEntity error(HttpServletRequest request, HttpServletResponse response) {
        HttpHeaders responseHeaders = new HttpHeaders();
        String path =request.getServletPath();
        String contextPath =request.getContextPath();

        HttpStatus httpStatus = org.springframework.http.HttpStatus.valueOf(response.getStatus());
        //return new ResponseEntity("Cannot Process Your Request Right Now! Please Contact the Administrator for more detail.", responseHeaders,httpStatus);
        ResponseEntity responseEntity = new ResponseEntity("Ok",HttpStatus.OK);
        return responseEntity;
    }

    @Override
    public String getErrorPath() {
        return errorPath;
    }

    @Autowired
    private ErrorAttributes errorAttributes;

    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return errorAttributes.getErrorAttributes((WebRequest) requestAttributes, includeStackTrace);
    }
}
