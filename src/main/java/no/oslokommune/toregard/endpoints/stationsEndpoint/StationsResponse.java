
package no.oslokommune.toregard.endpoints.stationsEndpoint;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "stations"
})
public class StationsResponse {

    @JsonProperty("stations")
    @Valid
    private List<Station> stations = new ArrayList<Station>();

    /**
     * No args constructor for use in serialization
     *
     */
    public StationsResponse() {
    }

    /**
     *
     * @param stations
     */
    public StationsResponse(List<Station> stations) {
        super();
        this.stations = stations;
    }

    @JsonProperty("stations")
    public List<Station> getStations() {
        return stations;
    }

    @JsonProperty("stations")
    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

}
