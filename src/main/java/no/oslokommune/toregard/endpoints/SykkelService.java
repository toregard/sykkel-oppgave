package no.oslokommune.toregard.endpoints;

import no.oslokommune.toregard.domain.Liste;

public interface SykkelService {
    Liste list(String clientIdentifier);
}
