package no.oslokommune.toregard.endpoints.AvailabilityEndpoint;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "stations",
        "updated_at",
        "refresh_rate"
})
public class AvailabilityResponse {

    @JsonProperty("stations")
    @Valid
    private List<Station> stations = null;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("refresh_rate")
    private Double refreshRate;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public AvailabilityResponse() {
    }

    /**
     *
     * @param updatedAt
     * @param stations
     * @param refreshRate
     */
    public AvailabilityResponse(List<Station> stations, String updatedAt, Double refreshRate) {
        super();
        this.stations = stations;
        this.updatedAt = updatedAt;
        this.refreshRate = refreshRate;
    }

    @JsonProperty("stations")
    public List<Station> getStations() {
        return stations;
    }

    @JsonProperty("stations")
    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("refresh_rate")
    public Double getRefreshRate() {
        return refreshRate;
    }

    @JsonProperty("refresh_rate")
    public void setRefreshRate(Double refreshRate) {
        this.refreshRate = refreshRate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
