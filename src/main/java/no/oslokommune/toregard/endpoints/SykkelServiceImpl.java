package no.oslokommune.toregard.endpoints;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.oslokommune.toregard.domain.Liste;
import no.oslokommune.toregard.domain.Rad;
import no.oslokommune.toregard.endpoints.AvailabilityEndpoint.AvailabilityResponse;
import no.oslokommune.toregard.endpoints.StatusEndpoint.StatusResponse;
import no.oslokommune.toregard.endpoints.stationsEndpoint.Station;
import no.oslokommune.toregard.endpoints.stationsEndpoint.StationsResponse;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

@Service
public class SykkelServiceImpl implements SykkelService {
    private static final String getAvailabilityEndpoint = "https://oslobysykkel.no/api/v1/stations/availability";
    private static final String getStatusEndpoint = "https://oslobysykkel.no/api/v1/status";
    private final RestTemplate restTemplate;
    private String clientIdentifier;

    @Inject
    public SykkelServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Liste list(String clientIdentifier) {
        this.clientIdentifier = clientIdentifier;
        return oppgaveTest();
    }

    public Liste oppgaveTest() {
        StationsResponse stations = getStations();
        AvailabilityResponse availabilityResponse = getAvailability();
        Liste liste = Liste.builder().rader(new HashMap<>()).build();
        for (no.oslokommune.toregard.endpoints.AvailabilityEndpoint.Station item : availabilityResponse.getStations()) {
            Station station = stations.getStations().stream().filter(o -> o.getId().equals(item.getId())).findFirst().orElse(null);
            if (station != null) {
                Rad rad = Rad.builder()
                        .antallLedigeSykler(item.getAvailability().getBikes())
                        .antallTilgjengeligeLaaser(item.getAvailability().getLocks())
                        .navnPaaStativ(station.getTitle()==null ? "Jackson problem" : station.getTitle() )
                        .build();
                liste.getRader().put(item.getId(), rad);
            }

        }
        System.out.println("stasjonene | Ledige sykler  |  Antall tilgj. låser");
        liste.getRader().forEach(
                (k, v) -> System.out.println(String.format("%d %s %s %s", k, v.getNavnPaaStativ(), v.getAntallLedigeSykler(), v.getAntallTilgjengeligeLaaser()))
        );
        return liste;
    }

    private AvailabilityResponse getAvailability() {
        HttpEntity<AvailabilityResponse> response = restTemplate.exchange(getAvailabilityEndpoint, HttpMethod.GET, getHttpEntity(), AvailabilityResponse.class);
        return response.getBody();
    }

    private StatusResponse getStatus() {
        HttpEntity<StatusResponse> response = restTemplate.exchange(getStatusEndpoint, HttpMethod.GET, getHttpEntity(), StatusResponse.class);
        return response.getBody();
    }

    /**
     * @Todo Vil ikke mappe String vi resttemplate
     * @return
     */
    private StationsResponse getStationsRest() {
        HttpEntity<StationsResponse> response = restTemplate.exchange(getAvailabilityEndpoint, HttpMethod.GET, getHttpEntity(), StationsResponse.class);
        return response.getBody();
    }


    public StationsResponse getStations() {
        StationsResponse stations = null;
        ClassLoader classLoader = SykkelServiceImpl.class.getClassLoader();
        try {
            File dataFile = new File(classLoader.getResource("Stations-data.json").getFile());
            ObjectMapper objectMapper = new ObjectMapper();
            stations = objectMapper.readValue(dataFile, StationsResponse.class);
        } catch (IOException iOException) {
            return null;
        }
        return stations;
    }


    private HttpEntity getHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Client-Identifier", clientIdentifier);
        HttpEntity entity = new HttpEntity(headers);
        return entity;
    }

}
